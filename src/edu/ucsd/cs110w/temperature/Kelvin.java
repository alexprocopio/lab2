package edu.ucsd.cs110w.temperature;
public class Kelvin extends Temperature
{
	float value = getValue();
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		String s  = Float.toString(value);
		return s + " K";
	}
	@Override
	public Temperature toCelsius()
	{
		float k = super.getValue();
		k = (k - 273); 
		return new Kelvin(k);
	}
	@Override
	public Temperature toFahrenheit()
	{
		float k = super.getValue();
		k = (k - 273);
		k = ((k * (1.8f)) + 32.0f);
		return new Kelvin(k);
	}
	@Override
	public Temperature toKelvin()
	{
		Kelvin k = new Kelvin(value);
		return k;
	}
}