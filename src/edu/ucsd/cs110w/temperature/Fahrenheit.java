/**
 * 
 */
package edu.ucsd.cs110w.temperature;
public class Fahrenheit extends Temperature
{
	float value = getValue();
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		String s  = Float.toString(value);
		return s + " F";
	}
	@Override
	public Temperature toCelsius()
	{
		float f = super.getValue();
		f = (f - 32.00f) * (5.00f/9.00f); 
		return new Celsius(f);
	}
	@Override
	public Temperature toFahrenheit()
	{
		Fahrenheit f = new Fahrenheit(value);
		return f;
	}
	@Override
	public Temperature toKelvin()
	{
		float f = super.getValue();
		f = (f - 32.00f) * (5.00f/9.00f); 
		f = (f + 273); 
		return new Kelvin(f);
	}
}
