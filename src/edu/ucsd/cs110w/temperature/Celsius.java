/**
 * 
 */
package edu.ucsd.cs110w.temperature;
public class Celsius extends Temperature
{
	float value = getValue();
	public Celsius(float t)
	{
		super(t);
		///asadsadas
	}
	public String toString()
	{
		String s  = Float.toString(value);
		return s + " C";
		//return s;
	}
	@Override
	public Temperature toFahrenheit()
	{
		float c = super.getValue();
		c = ((c * (1.8f)) + 32.0f); 
		return new Fahrenheit(c);
	}
	@Override
	public Temperature toCelsius()
	{
		Celsius c = new Celsius(value);
		return c;
	}
	@Override
	public Temperature toKelvin()
	{
		float c = super.getValue();
		c = (c + 273); 
		return new Kelvin(c);
	}
	
}